//
#include <16F88.h>
#device ADC=16
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#use delay(crystal=4MHz)
#use I2C(slave,SDA=PIN_B1,SCL=PIN_B4,fast,noforce_sw,address=0x11)
#INT_SSP

void main ()
{
 char step, R,G,B;
   int32 cont1,cont2,cont3;
   int dato,t,sw;
   cont1=1;
   cont2=1;
   cont3=1;
   sw=1;
   dato=1;
   t=1;
   output_low(pin_b0);
   output_high(pin_b2);
   output_high(pin_b3);
      
   while (true)
   {
               
      
      if (i2c_poll())
      {                  
         dato=i2c_read();
        
         switch (dato)
         {
                        
            case 1:// encender rojo
                output_low(pin_b0);
                output_high(pin_b2);
                output_high(pin_b3);
                break;
            case 2:// encender verde
                output_high(pin_b0);
                output_low(pin_b2);
                output_high(pin_b3);
                break;
            case 3:// encender azul
                output_high(pin_b0);
                output_high(pin_b2);
                output_low(pin_b3);
                break;
            case 4: // encender naranja
                output_low(pin_b0);
                output_low(pin_b2);
                output_high(pin_b3);
                break;
            case 5: // encender violeta
                output_low(pin_b0);
                output_high(pin_b2);
                output_low(pin_b3);
                break;
          
          
             case 6: // balanceo blanco
             while (dato==6)
             {      // baja intensidad
               output_high(pin_b0);
               output_high(pin_b2);
               output_high(pin_b3);
               switch (sw)
               {
                  case 1:
                    cont2=1;
                    step=1;
                    B=255; // nivel lum�nico de inicio
            
            
                    while (cont1<32000)
                    {
                       dato=i2c_read();
                       if (dato!=6)break;
                       if(B>=step)output_low(pin_b3);else output_high(pin_b3);
                       step++;                    //incrementar paso
                       if(!step) step=2;
                       B=B+1;
                       cont1=cont1+1;
                       if (cont1>32000)
                       {
                         sw=0;
                       }
                     }
             
                  case 0:
                  cont1=1;
                  step=1;
                  B=255; // nivel lum�nico de inicio
                  while (cont2<32000)// sube intensidad
                  {
                     dato=i2c_read();
                     if (dato!=6) break;
                     if(B>=step)output_high(pin_b3);else output_low(pin_b3);
                     
                     step++;                    //incrementar paso
                     if(!step) step=2;     
                     B=B+1;
                     cont2=cont2+1;
                     if (cont2>32000)
                     {                 
                        sw=1;
                      }
                  }
               }
             }// fin case 6
             break; 
              
         }
       }
     }
   }

 



